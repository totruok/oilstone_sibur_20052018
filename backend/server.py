from flask import Flask, request, send_from_directory
from flask_cors import CORS, cross_origin
import requests
import json
import os
import pandas as pd
import hashlib
import random
import datetime
import time
import numpy as np
import lightgbm as lgb
from tsfresh.feature_extraction import MinimalFCParameters
from tsfresh import extract_features
from sklearn.preprocessing import MinMaxScaler
import shap

app = Flask(__name__, static_url_path='/static')
cors = CORS(app)

def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

@app.route('/')
def hello():
    return 'api online'

@app.route('/set_time/', methods=['GET'])
def set_time():
    global global_offset
    
    args = request.args
    if 'time' in args:
        try:
            global_offset = int(args['time']) - int(time.time() * 20)
        except ValueError:
            pass
    return json.dumps({'offset': global_offset})

def compute_local_feature_impact(features_values, prob, feature_names):
    shap_values = shap.TreeExplainer(model).shap_values(features_values).flatten()[:-1]
    var_type = list(map(lambda s: s.split('__')[0] , feature_names))
    var_type = np.array(var_type)
    var_imp = []
    for t in np.unique(var_type):
        var_imp.append((t,shap_values[var_type==t].mean()))
    importance = pd.DataFrame(data= var_imp, columns =['variable','shap_score'])
    importance = importance.iloc[np.argsort(np.abs(importance['shap_score']))[::-1]]
    
    top_feat = importance[:5].copy()
    top_feat['shap_score'] /= top_feat['shap_score'].sum()/prob
    return list(top_feat['variable'].ravel()), list(top_feat['shap_score'].ravel()*100)
        
        
def predict(model, data):
    MAX_ROUNDS = 1000
    X = data.copy()
    X['id'] = 0
    features_extracted = extract_features(
        X,
        column_sort='date',
        column_id='id',
        default_fc_parameters=MinimalFCParameters(),
        disable_progressbar=True
    )
    
    pred = model.predict(features_extracted.values, num_iteration=model.best_iteration or MAX_ROUNDS)
    return (pred[0], *compute_local_feature_impact(features_extracted.values[0][None,:], pred[0],
                                                        features_extracted.columns))

@app.route('/get_all_data/')
def get_all_data():
    lookbehind_length = datetime.timedelta(minutes=60)
    fake_second = (int(time.time() * 20) + global_offset) % (seconds_in_sample - lookbehind_length.total_seconds())
    current_time = begin + lookbehind_length + datetime.timedelta(seconds=fake_second)
    interval_begin = current_time - lookbehind_length
    selected_interval = df[(df.date > interval_begin) & (df.date <= current_time)]
    columns = selected_interval.columns
    condition_of_parts = []
    prob_failure_in_5min,top_features,feature_impact = predict(model, selected_interval)
    scaled_features = scaler.transform(selected_interval.drop('date', axis=1).iloc[-1:])[0]
    for i in PARTS:
        wear = ((1065 - (current_time - i['times']).days) / 1065.0)*100
        condition_of_parts.append(
            {
                'times': str(i['times']),
                'name': i['name'],
                'wear': "{0:.2f}".format(wear)
            }
        )
    data = {
        'system_time': str(current_time),
        'current_predict':
            {
                "value": 100 * prob_failure_in_5min,
                "system_expertise_value": random.randint(85, 90),
                "factors_that_increase_the_probability_of_breakage": [
                    {
                        "text": "Из-за {}".format(top_features[i]),
                        "value": feature_impact[i],
                    } for i in range(len(top_features)) # отсортить и взвесить так что бы в сумме давало сумму предикта
                ],
            },
        "camera": 
            {
                "rtsp": "rtsp://frunza.kalan.cc:13054/tcp_live/ch0_0",
                "values": random.randint(0, 10) > 8,
            },
        "condition_of_parts": condition_of_parts, # отсортить по размеру wear
        "max_deviation_sensor_chart": [random.random() for i in range(240)],
        
        "left_charts": [
            {
                "name": "Нажим ножей",
                "value": random.randint(0,100),
                "name": [random.random() for i in range(40)],
            }
            for i in range(6)
        ],
        
        "sensors": [
            {
                "name": name,
                "values": feature * 100,
            }
            for name, feature in zip([colname for colname in columns if colname != 'date'], scaled_features)
        ]
    }
    return json.dumps(data)

PARTS = [
    {
        "name": "Холодильная жаровня",
        "times": pd.Timestamp('2016-{:02}-{:02} 00:00:00'.format(random.randint(1,12), random.randint(1,28))),
    },
    {
        "name": "Холодильная жаровня2",
        "times": pd.Timestamp('2016-{:02}-{:02} 00:00:00'.format(random.randint(1,12), random.randint(1,28))),
    },
    {
        "name": "Холодильная жаровня3",
        "times": pd.Timestamp('2016-{:02}-{:02} 00:00:00'.format(random.randint(1,12), random.randint(1,28))),
    },
    {
        "name": "Холодильная жаровня4",
        "times": pd.Timestamp('2016-{:02}-{:02} 00:00:00'.format(random.randint(1,12), random.randint(1,28))),
    },
]
df = pd.read_hdf('../../data/cleaned_dataset_utf.h5')
begin = pd.Timestamp('2017-12-01 00:00:00')
end = pd.Timestamp('2017-12-31 23:59:50')
df = df.drop('index', 1)
df = df[(df['date'] > begin) & (df['date'] < end)]
scaler = MinMaxScaler()
scaler.fit(df.drop('date', axis=1))
seconds_in_sample = len(df) * 10
global_offset = 0
model = lgb.Booster(model_file='/notebooks/oilstone_sibur/oilstone_sibur_20052018/notebooks/model-lookahead.bin')
app.run(host='0.0.0.0', debug=True, port=10014)

