import React, { Component } from 'react';
import { AreaChart, Area} from 'recharts';
import '.././App.css';

export default class MenuChart extends Component {
  render() {

    let value = 0;
    let data = [];
    let name = "";
    if (this.props.data && this.props.data.value && this.props.data.value.length > 0) {
      value = parseInt(this.props.data.value[0] * 100);
      name = this.props.data.name;
      data =this.props.data.value.map(function(num) {
        return {p: num};
      })
    }

    return (
      <div>
        <div className="menu-chart-block-top">
          <p className="menu-chart-title">{name}</p>
          <p className="menu-chart-value">{value}</p>
        </div>
        <div className="menu-chart-block-bottom">
          <AreaChart width={300} height={75} data={data} margin={{ top: 0, right: 0, bottom: 0, left: 0 }}>
            <defs>
              <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#232527" stopOpacity={0.3}/>
                <stop offset="95%" stopColor="#232527" stopOpacity={0.3}/>
              </linearGradient>
            </defs>
            <Area type="monotone" dataKey="p" stroke="#23252700" fillOpacity={0.3} fill="url(#colorUv)" />
          </AreaChart>
        </div>

      </div>

    );
  }
}