import React, { Component } from 'react';
import '.././App.css';

export default class VerticalBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      max: 160,
      min: 0,
    };

  }

  static colorByPercentage(value) {
    let color;

    if (value >= 0) {
      color = '#568855'
    }
    if (value >= 0.5) {
      color = '#B59C32'
    }
    if (value >= 0.8) {
      color = '#AB5455'
    }
    return color
  }

  render() {
    const {max} = this.state;
    const value = this.props.data? parseInt(this.props.data.values): 0;
    const title = this.props.data? this.props.data.name.slice(0,10): "";
    const percentage = (value / max);
    const height = percentage * 100 + "%";

    return (
      <div className="vertical-bar">
        <span className="vertical-bar-title">{title}</span>
        <div className="vertical-bar-back">
          <div className="vertical-bar-fill" style={{height: height, background: VerticalBar.colorByPercentage(percentage)}}/>
          <p className="vertical-bar-value">{value}</p>
        </div>

      </div>

    );
  }
}
