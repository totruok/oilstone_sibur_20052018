import React, {Component} from 'react';
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'

require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/solid-gauge')(Highcharts);

export default class Speed extends Component {

    render() {
      var options = {

        chart: {
          type: 'solidgauge',
          height: "300px",
        },

        title: {
          text: 'Вероятность останова<br/>' + this.props.date
        },

        pane: {
          center: ['50%', '55%'],
          size: '100%',
          startAngle: -90,
          endAngle: 90,
          border: 0,
          background: {
            backgroundColor: '#373B40',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
          }
        },
        tooltip: {
          enabled: false
        },
        yAxis: {
          stops: [
            [0.1, '#568855'], // green
            [0.5, '#B59C32'], // yellow
            [0.9, '#DF5353'] // red
          ],
          lineWidth: 0,
          minorTickInterval: null,
          tickAmount: 2,
          labels: {
            enabled: false,
          },
          min: 0,
          max: 100,
        },

        plotOptions: {
          solidgauge: {
            dataLabels: {
              y: 5,
              borderWidth: 0,
              useHTML: true
            }
          }
        },

        series: [{
          data: [this.props.value2],
          dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:15px;color:white">{y:.7f}%</span></div>',
            y: -30
          },
          tooltip: {
            valueSuffix: ' revolutions/min'
          }
        }]
      };

        const chart = <HighchartsReact
          highcharts={Highcharts}
          constructorType={'chart'}
          options={options}
        />;

        return chart
    }
}