import React, { Component } from 'react';
import '.././App.css';

export default class Camera extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title? this.props.title: "",
      max: 100,
      min: 0,
      value: 0,
    };

  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.update(),
      3000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  update() {
    this.setState({
      value: this.random()
    });
  }

  random()
  {
    return Math.floor(Math.random()*(this.state.max-this.state.min+1)+this.state.min);
  }
  static colorByPercentage(value) {
    let color;

    if (value >= 0) {
      color = '#568855'
    }
    if (value >= 0.5) {
      color = '#B59C32'
    }
    if (value >= 0.8) {
      color = '#AB5455'
    }
    return color
  }

  render() {
    const {title, value, max} = this.state;
    const percentage = (value / max);
    const height = percentage * 100 + "%";



    return (
      <div className="vertical-bar">
        <span className="vertical-bar-title">{title}</span>
        <div className="vertical-bar-back">
          <div className="vertical-bar-fill" style={{height: height, background: VerticalBar.colorByPercentage(percentage)}}/>
          <p className="vertical-bar-value">{value}</p>
        </div>

      </div>

    );
  }
}
