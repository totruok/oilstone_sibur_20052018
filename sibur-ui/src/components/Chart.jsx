import React, {Component} from 'react';
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'

const c = {
    updateFunction: function () {
      let x = (new Date()).getTime();
      return [x, this.y]
    },
    y: 0
}
const options = {
    chart: {
        type: 'spline',
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
            load: function () {

                // set up the updating of the chart each second
                var series = this.series[0];
                setInterval(function () {
                    series.addPoint(c.updateFunction(), true, true);
                }, 1000);
            }
        },height: "300px",

    },
    title: {
        text: 'Вероятность отказа:<br>'
    },
    xAxis: {
        type: 'datetime',
        tickPixelInterval: 150
    },
    yAxis: {
        title: {
            text: ''
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }],
      min: 0,
      max: 100,
    },

  tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                Highcharts.numberFormat(this.y, 2);
        }
    },
    legend: {
        enabled: false
    },
    exporting: {
        enabled: false
    },
    series: [{
        data: (function () {
            // generate an array of random data
            var data = [],
                time = (new Date()).getTime(),
                i;

            for (i = -40; i <= 0; i += 1) {
                data.push({
                    x: time + i * 1000,
                    y: 0
                });
            }
            return data;
        }())
    }]
};


export default class Chart extends Component {

    render() {
      //   console.log(this.props.value);
      //   c.updateFunction = function () {
      //   let x = (new Date()).getTime(), // current time
      //     y = 1;
      //   return [x, y]
      // };
        c.y = this.props.value;
        console.log(c.y );

        return <HighchartsReact
            highcharts={Highcharts}
            constructorType={'chart'}
            options={options}
        />
    }

}