import React, { Component } from 'react';
import '.././App.css';

export default class HorizontalBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title? this.props.title: "",
      max: 200,
      min: 0,
    };

  }

  static colorByPercentage(value) {
    let color;

    if (value >= 0) {
      color = '#568855'
    }
    if (value >= 0.5) {
      color = '#B59C32'
    }
    if (value >= 0.8) {
      color = '#AB5455'
    }
    return color
  }

  render() {
    const {max} = this.state;
    const value = this.props.data? parseInt(this.props.data.values): 0;
    const title = this.props.data? this.props.data.name.slice(0,10): "";
    const percentage = (value / max);
    const width = percentage * 100 + "%";


    return (
      <div className="horizontal-bar">
        <span className="horizontal-bar-title">{title}</span>
        <div className="horizontal-bar-back">
          <div className="horizontal-bar-fill" style={{width: width, background: HorizontalBar.colorByPercentage(percentage)}}/>
        </div>

      </div>

    );
  }
}
