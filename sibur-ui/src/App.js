import React, {Component} from 'react';
import './App.css';
import Highcharts from 'highcharts/highstock'
import Chart from "./components/Chart";
import {theme} from "./components/Theme";
import MenuChart from "./components/MenuChart";
import VerticalBar from "./components/VerticalBar";
import HorizontalBar from "./components/HorizontalBar";
import Speed from "./components/Speed";
import Backend from "./api/Backend";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.update(),
      2000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  update() {
    Backend.update(data => {
      console.log(data);
      if (data) {
        this.setState({data: data})
      }

    })
  }

  render() {

    Highcharts.setOptions(theme);

    let miniChart = {name:"",value:[]};
    let miniChart2 = {name:"",value:[]};
    let miniChart3 = {name:"",value:[]};
    let miniChart4 = {name:"",value:[]};
    let miniChart5 = {name:"",value:[]};
    let miniChart6 = {name:"",value:[]};

    if (this.state.data && this.state.data['left_charts']) {
      console.log("left");
      console.log(this.state.data['left_charts'][0]);
      miniChart = this.state.data['left_charts'][0];
      miniChart2 = this.state.data['left_charts'][1];
      miniChart3 = this.state.data['left_charts'][2];
      miniChart4 = this.state.data['left_charts'][3];
      miniChart5 = this.state.data['left_charts'][4];
      miniChart6 = this.state.data['left_charts'][5];
    }

    let current_predict = {
      factors: [],
      "system_expertise_value": 0,
      value: 0,
      value2: 0
    };

    let sensors = Array.apply(0, Array(44)).map(function (num) {
      return {
        name: "",
        values: num}
    });

    let date = "";
    if (this.state.data && this.state.data['system_time']) {
      date = this.state.data['system_time']
    }
    let conditions = [];
    let statusPercentage = '0.1';

    if (this.state.data && this.state.data['current_predict']) {
      current_predict.factors = this.state.data['current_predict']['factors_that_increase_the_probability_of_breakage'];
      current_predict.value2 = this.state.data['current_predict'].value;
      current_predict.value = parseInt(this.state.data['current_predict'].value);
      if (current_predict.value > 50) {
        statusPercentage = '1.0'
      } else {
        statusPercentage = '0.1'
      }
    }

    if (this.state.data && this.state.data['condition_of_parts']) {
      conditions = this.state.data['condition_of_parts'];
    }

    if (this.state.data && this.state.data['sensors']) {
      sensors = this.state.data['sensors']
    }

    return (
      <div className="App">
        <div className="left">
          <div className="menu-chart-block">
            <MenuChart data={miniChart}/>
          </div>
          <div className="menu-chart-block">
            <MenuChart data={miniChart2}/>
          </div>
          <div className="menu-chart-block">
            <MenuChart  data={miniChart3}/>
          </div>
          <div className="menu-chart-block">
            <MenuChart data={miniChart4}/>
          </div>
          <div className="menu-chart-block">
            <MenuChart data={miniChart5}/>
          </div>
          <div className="menu-chart-block">
            <MenuChart data={miniChart6}/>
          </div>
          <div className="menu-chart-block">
          </div>
        </div>
        <div className="right">
          <table className="table-top">
            <tr>
              <td>
                <div className="table-top-block-left">
                  <div className="top-line-red"/>
                  <table className="table-top-inside">
                    <tr>
                      <td>
                        <div className="main-indicator-block">
                          <div className="speed-indicator-wrapper">
                            <Speed value={current_predict.value} value2={current_predict.value2} date={date}/>
                          </div>
                          <div className="problem-indicator-wrapper">
                            <span className="problem-indicator" style={{color: 'rgba(255,255,255,' + statusPercentage +')', backgroundColor: 'rgba(171,84,85,' + statusPercentage +')'}}>Критическое нарушение</span>
                          </div>
                        </div>
                      </td>
                      <td className="top-cell">
                        <div>
                          <table className="fits-table" cellSpacing={0} cellPadding={0}>
                            <tbody>
                            <tr>
                              <td>Причина</td>
                              <td> </td>
                              <td>Подтверждение</td>
                            </tr>
                            {current_predict.factors.map((factor) =>
                              <tr>
                                <td>{factor.text}</td>
                                <td>~ {parseInt(factor.value).toString() + "%"}</td>
                                <td><span className="btn-plus">+</span><span className="btn-minus">-</span></td>
                              </tr>
                            )}
                            </tbody>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
              <td>
                <div className="table-middle-block">

                  <img style={{height: '100%', width:'100%'}} src="https://i.imgur.com/7XFVWiy.gif" alt="" />
                </div>
              </td>
            </tr>
          </table>

          <table className="table-middle">
            <tr>
              <td>
                <div className="table-middle-block">
                  <div className="top-line-blue"/>
                  <table className="fits-table" cellSpacing={0} cellPadding={0} style={{marginLeft: '40px'}}>
                    <tbody>
                    <tr>
                      <td>Наименование<br/> компанента</td>
                      <td>Износ<br/> оборудования</td>
                      <td>Дата установки<br/> нового</td>
                    </tr>
                    {conditions.map((condition) =>
                      <tr>
                        <td>{condition.name}</td>
                        <td>~ {parseInt(condition.wear).toString() + "%"}</td>
                        <td>{condition.times.slice(0,10)}</td>
                      </tr>
                    )}
                    </tbody>
                  </table>
                </div>
              </td>
              <td>
                <div className="table-middle-block">
                    <Chart value={current_predict.value2}/>
                </div>
              </td>
            </tr>
          </table>

          <div className="block-bottom">
            <table>
              <tr>
                <td>
                  <table className="bars">
                    <tr>
                      <td><VerticalBar data={sensors[0]}/></td>
                      <td><VerticalBar data={sensors[1]}/></td>
                      <td><VerticalBar data={sensors[2]}/></td>
                      <td><VerticalBar data={sensors[3]}/></td>
                    </tr>
                    <tr>
                      <td><VerticalBar data={sensors[4]}/></td>
                      <td><VerticalBar data={sensors[5]}/></td>
                      <td><VerticalBar data={sensors[6]}/></td>
                      <td><VerticalBar data={sensors[7]}/></td>
                    </tr>
                  </table>
                </td>
                <td>
                  <table>
                    <tr>
                      <td><HorizontalBar data={sensors[8]}/></td>
                      <td><HorizontalBar data={sensors[13]}/></td>
                      <td><HorizontalBar data={sensors[16]}/></td>
                      <td><HorizontalBar data={sensors[29]}/></td>
                      <td><HorizontalBar data={sensors[24]}/></td>
                      <td><HorizontalBar data={sensors[28]}/></td>
                      <td><HorizontalBar data={sensors[32]}/></td>
                      <td><HorizontalBar data={sensors[36]}/></td>
                    </tr>
                    <tr>
                      <td><HorizontalBar data={sensors[9]}/></td>
                      <td><HorizontalBar data={sensors[13]}/></td>
                      <td><HorizontalBar data={sensors[17]}/></td>
                      <td><HorizontalBar data={sensors[21]}/></td>
                      <td><HorizontalBar data={sensors[25]}/></td>
                      <td><HorizontalBar data={sensors[29]}/></td>
                      <td><HorizontalBar data={sensors[33]} /></td>
                      <td><HorizontalBar data={sensors[37]}/></td>
                    </tr>
                    <tr>
                      <td><HorizontalBar data={sensors[10]}/></td>
                      <td><HorizontalBar data={sensors[14]}/></td>
                      <td><HorizontalBar data={sensors[18]}/></td>
                      <td><HorizontalBar data={sensors[22]}/></td>
                      <td><HorizontalBar data={sensors[26]}/></td>
                      <td><HorizontalBar data={sensors[30]}/></td>
                      <td><HorizontalBar data={sensors[34]}/></td>
                      <td><HorizontalBar data={sensors[38]}/></td>
                    </tr>
                    <tr>
                      <td><HorizontalBar data={sensors[11]}/></td>
                      <td><HorizontalBar data={sensors[15]}/></td>
                      <td><HorizontalBar data={sensors[19]}/></td>
                      <td><HorizontalBar data={sensors[23]}/></td>
                      <td><HorizontalBar data={sensors[27]}/></td>
                      <td><HorizontalBar data={sensors[31]}/></td>
                      <td><HorizontalBar data={sensors[35]}/></td>
                      <td><HorizontalBar data={sensors[39]}/></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default App;

  /*<div style={{background: 'rgba(34,133,132,1)', height: '100%'}}/>*/