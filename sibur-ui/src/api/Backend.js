import axios from 'axios'

class Backend {

    constructor() {
    }

    update(listener) {
      axios.get('http://home.totruok.ru:45514/get_all_data/').then(response => {
        console.log(response.data);
        listener(response.data)
      }, (error) => {
        console.log(error);
      })
    }
}

export default new Backend();